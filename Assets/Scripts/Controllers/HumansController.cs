﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class HumansController
{
    public Action<int> OnTargetReached = null;
    private SpawnData _spawnData;
    private InterractPointsData _interractData;
    private GameObject _humanTemplate;

    private List<Vector3> _openedInteractPoints = new List<Vector3>();
    private List<Vector3> _closedInteractPoints = new List<Vector3>();

    private List<Human> _humans = new List<Human>();

    UIProgressBarBubblesController bubbles;
    public HumansController(UIProgressBarBubblesController _bubbles)
    {
        bubbles = _bubbles;
           TextAsset spawnPointsAsset = Resources.Load("spawnData") as TextAsset;
        _spawnData = JsonUtility.FromJson<SpawnData>(spawnPointsAsset.text);
        TextAsset interractPointsAsset = Resources.Load("interactPoints") as TextAsset;
        _interractData = JsonUtility.FromJson<InterractPointsData>(interractPointsAsset.text);
        _openedInteractPoints = _interractData.points;
        _humanTemplate = Resources.Load("human") as GameObject;

        SpawnHumans();
    }

    void SpawnHumans()
    {
        for (int i = 0; i < _spawnData.points.Count; i++)
        {
            Vector3 target = _openedInteractPoints[UnityEngine.Random.Range(0, _openedInteractPoints.Count - 1)];
            GameObject _humanObject = GameObject.Instantiate(_humanTemplate, _spawnData.points[i], Quaternion.identity);
            Human _human = new Human(_humanObject);
            SetInteractPointToHuman(_human, target);
            _humans.Add(_human);

        }
    }

    void SetInteractPointToHuman(Human human, Vector3 target)
    {
        _openedInteractPoints.Remove(target);
        _closedInteractPoints.Add(target);
        human.StartMove(target);
    }

    public void Update()
    {
        for (int i = 0; i < _humans.Count; i++)
        {
            if (!_humans[i].agent.pathPending)
            {
                if (_humans[i].agent.remainingDistance <= _humans[i].agent.stoppingDistance)
                {
                    if (!_humans[i].agent.hasPath || _humans[i].agent.velocity.sqrMagnitude == 0f)
                    {
                        if (!_humans[i].isDoSomeThing)
                        {
                            UIProgressBarBubble bubble;
                            _humans[i].StopMove();
                            bubbles.ShowProgressBarBubble(_humans[i].humanObject.transform, out bubble);
                            _humans[i].bubble = bubble;
                        }
                        else
                        {
                            _humans[i].Chill();
                            if (_humans[i].isChillComplete)
                            {
                                Vector3 prewTarget = _humans[i].agent.destination;
                                Vector3 target = _openedInteractPoints[UnityEngine.Random.Range(0, _openedInteractPoints.Count - 1)];
                                SetInteractPointToHuman(_humans[i], target);
                                if (OnTargetReached != null)
                                {
                                    OnTargetReached(200);
                                }
                                _openedInteractPoints.Add(prewTarget);
                                _closedInteractPoints.Remove(prewTarget);
                            }
                        }
                    }
                }
            }
        }
    }
}
