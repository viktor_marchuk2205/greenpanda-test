using System.Collections.Generic;
using UnityEngine;

public class UIProgressBarBubblesController : MonoBehaviour
{
	public Transform parent;
	public UIProgressBarBubble	progressBubble;
    private List<UIProgressBarBubble> _progressBubbles = new List<UIProgressBarBubble>();

	private void Awake()
	{
		_progressBubbles.Add(progressBubble);
	}

    public void ShowProgressBarBubble(Transform humanTransform, out UIProgressBarBubble bubble)
    {
        bubble = _progressBubbles.Find(e => e.gameObject.activeInHierarchy == false);

        if (bubble == null)
        {
            bubble = GameObject.Instantiate(progressBubble);

            _progressBubbles.Add(bubble);
            bubble.transform.SetParent(parent);
        }

        bubble.Show(humanTransform);
    }
}