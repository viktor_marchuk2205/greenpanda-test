﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraTouchController : MonoBehaviour
{

    public Camera _camera;
    public float maxOrthograpicSize;
    public float minOrthograpicSize;
    public float zoomSpeed;
    public float zoomLerpValue;
    private Vector3 dragOrigin;
    private Vector3 clickOrigin = Vector3.zero; 
    private Vector3 basePos = Vector3.zero;
    private float _targetOrthograpicSize;

    private void Awake()
    {
        _targetOrthograpicSize = _camera.orthographicSize;
    }

    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            _targetOrthograpicSize -= zoomSpeed;
            _targetOrthograpicSize = Mathf.Clamp(_targetOrthograpicSize, minOrthograpicSize, maxOrthograpicSize);
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            _targetOrthograpicSize += zoomSpeed;
            _targetOrthograpicSize = Mathf.Clamp(_targetOrthograpicSize, minOrthograpicSize, maxOrthograpicSize);


        }
        _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _targetOrthograpicSize, zoomLerpValue * Time.deltaTime);

        
        if (Input.GetMouseButton(0)) {
            if (clickOrigin == Vector3.zero)
            {
                clickOrigin = Input.mousePosition;
                basePos = transform.position;
            }
            dragOrigin = Input.mousePosition;
        }

        if (!Input.GetMouseButton(0)) {
            clickOrigin = Vector3.zero;
            return;
        }

       

        transform.position = Vector3.Lerp(basePos, new Vector3(basePos.x + ((clickOrigin.x - dragOrigin.x) * .05f), basePos.y + ((clickOrigin.y - dragOrigin.y) * .05f), -10),0.5f);//new Vector3(basePos.x + ((clickOrigin.x - dragOrigin.x) * .05f), basePos.y + ((clickOrigin.y - dragOrigin.y) * .05f), -10);
    }
}
