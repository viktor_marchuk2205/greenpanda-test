﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Human
{
    public GameObject humanObject;
    public NavMeshAgent agent;
    public Animator anim;
    public UIProgressBarBubble bubble;
    public bool isDoSomeThing = false;
    public bool isChillComplete = false;

    private float _chillOutTime = 5f;

    public Human(GameObject _humanObject)
    {
        humanObject = _humanObject;
        agent = humanObject.GetComponent<NavMeshAgent>();
        anim = humanObject.GetComponent<Animator>();
    }

    public void StartMove(Vector3 target)
    {
        agent.destination = target;
        anim.ResetTrigger("idle");
        anim.SetTrigger("walk");
        _chillOutTime = 5f;
        isChillComplete = false;
    }

    public void StopMove()
    {
        isDoSomeThing = true;
        anim.ResetTrigger("walk");
        anim.SetTrigger("idle");
    }

    public void Chill()
    {
        _chillOutTime -= Time.deltaTime;
        if (bubble != null)
        {
            bubble.UpdateSlider((5f - _chillOutTime) * 0.2f);
        }
        if (_chillOutTime <= 0)
        {
            isDoSomeThing = false;
            isChillComplete = true;
            if (bubble != null)
            {
                bubble.Hide();
            }
        }
    }

}

