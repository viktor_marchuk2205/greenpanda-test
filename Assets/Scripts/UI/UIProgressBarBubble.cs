using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class UIProgressBarBubble : UIMapAttachedElement {

    public Image slider;
    public float distToSpots = 1;

    public void Show(Transform transform) 
    {
        slider.fillAmount = 0;
        SetTarget(transform, new Vector3(-1,2f,0));
        this.transform.localScale = Vector3.one;
        gameObject.SetActive(true);
    }

    public void UpdateSlider(float value)
    {
        slider.fillAmount = value;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        slider.fillAmount = 0;
    }


	float dist;
    public override void LateUpdate()
    {
        base.LateUpdate();

        _screenPoint = _cachedCameraController.WorldToScreenPoint(GetMapPosition()); 

        dist = -(_rectTransform.position - _screenPoint).normalized.z;
    }

}