using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIMapAttachedElement : MonoBehaviour
{
    public RectTransform parentRectTransform;
    public RectTransform elementRectTransform;

    public Camera _cachedCameraController;
    private Transform _targetTransform;
    private Vector3 _offset;

	protected RectTransform _rectTransform;
	protected Vector3 _screenPoint;
	Rect _parentRect;

    void OnEnable()
    {
        _cachedCameraController = Camera.main;

		_rectTransform = transform as RectTransform;
		_parentRect = parentRectTransform.rect;
    }

    public void SetTarget(Transform targetTransform, Vector3 offset = default(Vector3))
    {
        _targetTransform = targetTransform;
        _offset = offset;
    }

    protected Vector3 GetMapPosition()
    {
        return _targetTransform == null? _offset : _targetTransform.position + _offset;
    }

    protected virtual void UpdateState()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        if (!_cachedCameraController)
            _cachedCameraController = Camera.main;

        _screenPoint = _cachedCameraController.WorldToScreenPoint(GetMapPosition()); 

        _rectTransform.anchoredPosition = new Vector2(_parentRect.width * _screenPoint.x / Screen.width, _parentRect.height * _screenPoint.y / Screen.height);
    }

    public virtual void LateUpdate()
    {
        UpdateState();
    }

}
