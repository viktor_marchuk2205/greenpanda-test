﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameUIManager : MonoBehaviour
{
    public TMP_Text coinsAmount;

    public void UpdateCoins(int amount)
    {
        coinsAmount.text = amount.ToString();
    }
}
