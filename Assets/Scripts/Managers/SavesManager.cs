﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class StorageData
{
    public int coins;
}

public class SavesManager
{
    public StorageData data = null;

    private const string SAVE_FILENAME = "{0}/Saves.json";

    public SavesManager()
    {
        SetLocalStorage();
    }

    public void UpdateCoins(int coinsAmount)
    {
        data.coins += coinsAmount;
    }

    string GetFilePath(string fileName)
    {
        return string.Format(fileName, Application.persistentDataPath);
    }

    void SetLocalStorage()
    {
        if (File.Exists(GetFilePath(SAVE_FILENAME)))
        {
            byte[] bytes = File.ReadAllBytes(GetFilePath(SAVE_FILENAME));

            string str = Encoding.UTF8.GetString(bytes);

            data = JsonUtility.FromJson<StorageData>(str);
        }
        else
        {
            data = new StorageData();
            byte[] bytesToWrite = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data, true));
            WriteToFile(bytesToWrite);
        }
    }

    void WriteToFile(byte[] bytes)
    {
        FileStream fileStream = File.Create(GetFilePath(SAVE_FILENAME));

        fileStream.Write(bytes, 0, bytes.Length);
        fileStream.Flush();
        fileStream.Close();
    }

    public void SaveAll()
    {
        byte[] bytesToWrite = Encoding.UTF8.GetBytes(JsonUtility.ToJson(data, true));
        WriteToFile(bytesToWrite);
    }

}
