﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameUIManager _gameUIManager;
    private SavesManager _savesManager;
    private HumansController _humanController;

    public UIProgressBarBubblesController bubbles;

    private void Awake()
    {
        _savesManager = new SavesManager();
        _humanController = new HumansController(bubbles);
        _gameUIManager.UpdateCoins(_savesManager.data.coins);
        _humanController.OnTargetReached += OntargetReached;
    }

    private void Update()
    {
        _humanController.Update();
    }

    void OntargetReached(int amount)
    {
        _savesManager.UpdateCoins(amount);
        _gameUIManager.UpdateCoins(_savesManager.data.coins);
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            _savesManager.SaveAll();
        }
    }

    private void OnApplicationQuit()
    {
        _savesManager.SaveAll();
    }

    private void OnDestroy()
    {
        _humanController.OnTargetReached -= _savesManager.UpdateCoins;
    }
}
