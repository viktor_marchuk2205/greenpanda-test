﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnData 
{
    public List<Vector3> points = new List<Vector3>();
}
